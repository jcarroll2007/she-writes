(function () {
    'use strict';

    function config($stateProvider, $urlRouterProvider) {

        ///////////////////////////
        // Redirects and Otherwise
        ///////////////////////////
        $urlRouterProvider
            .otherwise('/');

        ///////////////////////////
        // State Configurations
        ///////////////////////////

        $stateProvider


            ///////////////////////////
            // App
            ///////////////////////////
            .state('app', {
                url: '/',
                templateUrl: 'app/app.html',
                abstract: true
            })

            ///////////////////////////
            // Home
            ///////////////////////////
            .state('app.home', {
                url: '',
                templateUrl: 'app/home/home.html',
                controllerAs: 'home',
                controller: function (instagram, $timeout) {
                    var self = this;

                    $timeout(function () {
                        self.fadeLogoIn = true;
                    });

                    instagram.getFeed()
                        .then(function (feed) {
                            self.feed = feed;

                            $timeout(function () {
                                self.fadeFeedIn = true;
                            }, 1000);
                        });
                }
            });
    }

    function init($rootScope) {
        $rootScope.debug = true;
    }

    angular.module('app', [
        'ui.router',
        'ngAnimate',
        'models'
    ])
        .config(config)
        .run(init);
}());