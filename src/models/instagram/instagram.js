(function () {
    'use strict';

    function instagram($http) {
        this.getFeed = function () {
            return $http.get('https://www.instagram.com/shewritesbyerica/media/')
                .then(function (feed) {
                    return feed;
                });
        }
    }

    angular.module('models.instagram', [])
        .service('instagram', instagram);
}());